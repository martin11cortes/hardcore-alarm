package co.edu.konradlorenz.hardcorealarm.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import co.edu.konradlorenz.hardcorealarm.R;
import co.edu.konradlorenz.hardcorealarm.entities.Alarma;
import co.edu.konradlorenz.hardcorealarm.fragments.ConfiguracionAlarmaFragment;
import co.edu.konradlorenz.hardcorealarm.fragments.ListadoAlarmasFragment;


public class ListadoAlarmasAdapter extends ArrayAdapter<Alarma> {

    public ListadoAlarmasAdapter(@NonNull Context context, int resource, @NonNull List<Alarma> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        final Alarma alarma = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.alarma_item, null);
        }

        TextView horaText = convertView.findViewById(R.id.horaText);
        TextView msgText = convertView.findViewById(R.id.mensajeAlarmaText);
        LinearLayout layout = convertView.findViewById(R.id.alarma);

        horaText.setText(String.format("%02d:%02d", alarma.getHour(), alarma.getMinute()));
        if ("".equals(alarma.getMessage())) {
            msgText.setText("No message text set");
        } else {
            msgText.setText(alarma.getMessage());
        }

        final Alarma objeto = getItem(position);
        final View finalconvertView = convertView;

        //Manejo del clic en cada alarma
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent newIntent = new Intent(getContext(), ConfiguracionAlarma.class);
                //Envío de la referencia de cada alarma.
                //newIntent.putExtra("objetoData", alarma);
                //-----
                //getContext().startActivity(newIntent);
//                Toast.makeText(getContext(), "Dí click en" + objeto.getHoraText(), Toast.LENGTH_LONG).show();

                ((FragmentActivity) getContext()).getSupportFragmentManager().beginTransaction()
                        .add(R.id.fragment_container, new ConfiguracionAlarmaFragment().newInstance(alarma)).addToBackStack(null)
                        .commit();

                //getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, new ListadoAlarmasFragment()).addToBackStack(null).commit();

                //new ConfiguracionAlarmaFragment().newInstance(alarma);
            }
        });

        return convertView;
    }
}
