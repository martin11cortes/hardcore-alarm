package co.edu.konradlorenz.hardcorealarm.entities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Random;

public class Alarma implements Serializable {

    private int hour;
    private int minute;
    private ArrayList days;
    private String message;
    private int id;
    private int volume;
    private boolean vibration;
//    private String sonidoSeleccionado;

    public Alarma() {
    }

    public Alarma(int hour, int minute, ArrayList days, String message, int volume, boolean vibration) {
        this.hour = hour;
        this.minute = minute;
        this.days = days;
        this.message = message;
        this.volume = volume;
        this.vibration = vibration;
        setId();
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public ArrayList getDays() {
        return days;
    }

    public void setDays(ArrayList days) {
        this.days = days;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public boolean isVibration() {
        return vibration;
    }

    public void setVibration(boolean vibration) {
        this.vibration = vibration;
    }

    public void setId() {
        Random randomNumberGenerator = new Random();
        id = randomNumberGenerator.nextInt(1000000);
    }

    public int getId() {
        return id;
    }
//
//    @Override
//    public void onReceive(Context context, Intent intent) {
//
//    }
}
