package co.edu.konradlorenz.hardcorealarm.fragments;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.ToggleButton;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;

import co.edu.konradlorenz.hardcorealarm.MainActivity;
import co.edu.konradlorenz.hardcorealarm.R;
import co.edu.konradlorenz.hardcorealarm.AlarmReciever;
import co.edu.konradlorenz.hardcorealarm.entities.Alarma;

public class ConfiguracionAlarmaFragment extends Fragment {
    private AlarmManager alarmManager;
    private PendingIntent pendingIntent;

    FirebaseDatabase database = FirebaseDatabase.getInstance();

    public static ConfiguracionAlarmaFragment newInstance(Alarma alarm) {
        ConfiguracionAlarmaFragment myFragment = new ConfiguracionAlarmaFragment();

        Bundle args = new Bundle();
        args.putString("hour", Integer.toString(alarm.getHour()));
        args.putString("minutes", Integer.toString(alarm.getMinute()));
        args.putString("volume", Integer.toString(alarm.getVolume()));
        args.putString("message", alarm.getMessage());
        args.putString("vibration", Boolean.toString(alarm.isVibration()));

        myFragment.setArguments(args);

        return myFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.configuracion_alarma_view, container, false);

        TextView title_edit_alarm = view.findViewById(R.id.title_edit_alarm);
        TextView textTimePicker = view.findViewById(R.id.textTimePicker);
        LinearLayout calendarLayout = view.findViewById(R.id.calendarDays);
        TextView messageView = view.findViewById(R.id.mensaje_alarma);
        SeekBar volumenView = view.findViewById(R.id.seekBarVolume);
        Switch alarmSwitch = view.findViewById(R.id.vibrationAlarm);
        Bundle bundle = this.getArguments();

        if (bundle != null) {
            Alarma savedAlarm = new Alarma(Integer.parseInt(bundle.getString("hour")),
                    Integer.parseInt(bundle.getString("minutes")),
                    new ArrayList(),
                    bundle.getString("message"),
                    Integer.parseInt(bundle.getString("volume")),
                    Boolean.parseBoolean(bundle.getString("vibration")));
            title_edit_alarm.setText(R.string.edit_alarm);
            textTimePicker.setText(String.format("%02d:%02d", savedAlarm.getHour(), savedAlarm.getMinute()));
            messageView.setText(savedAlarm.getMessage());
            volumenView.setProgress(savedAlarm.getVolume());
            alarmSwitch.setChecked(savedAlarm.isVibration());
        } else {
            title_edit_alarm.setText(R.string.create_alarm);
//            Calendar mcurrentTime = Calendar.getInstance();
//            int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
//            int minute = mcurrentTime.get(Calendar.MINUTE);
//            textTimePicker.setText(String.format("%02d:%02d", hour, minute));
        }

        final TextView alarmTimeText = view.findViewById(R.id.textTimePicker);
        alarmTimeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(v.getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        final String timeText = String.format("%02d:%02d", selectedHour, selectedMinute);
                        alarmTimeText.setText(timeText);
                    }
                }, hour, minute, true);
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });

        alarmManager = (AlarmManager) getContext().getSystemService(MainActivity.ALARM_SERVICE);

        final Calendar calendar = Calendar.getInstance();

        final Intent intent = new Intent(getContext(), AlarmReciever.class);

        final DatabaseReference myRef = database.getReference("alarms");

        final Button createAlarm = view.findViewById(R.id.saveAlarm);
        createAlarm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View viewButton) {

                TextView fullTime = view.findViewById(R.id.textTimePicker);
                String[] textTime = fullTime.getText().toString().split(":");
                int hour = Integer.parseInt(textTime[0]);
                int minutes = Integer.parseInt(textTime[1]);
                ArrayList calendarArray = new ArrayList();
                LinearLayout calendarLayout = view.findViewById(R.id.calendarDays);
                int count = calendarLayout.getChildCount();
                for (int i = 0; i < count; i++) {
                    ToggleButton day = (ToggleButton) calendarLayout.getChildAt(i);
                    calendarArray.add(day.isChecked());
                }
                TextView messageView = view.findViewById(R.id.mensaje_alarma);
                String message = messageView.getText().toString();

                SeekBar volumenView = view.findViewById(R.id.seekBarVolume);
                int alarmVolume = volumenView.getProgress();

                Switch alarmSwitch = view.findViewById(R.id.vibrationAlarm);
                boolean vibration = alarmSwitch.isChecked();

                Alarma newAlarm = new Alarma(hour, minutes, calendarArray, message, alarmVolume, vibration);

                myRef.child(Integer.toString(newAlarm.getId())).setValue(newAlarm);

                getActivity().getFragmentManager().popBackStackImmediate();
//                calendar.set(Calendar.HOUR_OF_DAY, alarma.getHoraText());
//                calendar.set(Calendar.MINUTE, timePicker.getMinute());

//                pendingIntent = PendingIntent.getBroadcast(getContext(), 0, intent, PendingIntent.FLAG_IMMUTABLE);

//                alarmManager.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), pendingIntent);
            }
        });


        return view;
    }
}
