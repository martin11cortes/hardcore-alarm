package co.edu.konradlorenz.hardcorealarm.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import co.edu.konradlorenz.hardcorealarm.R;

public class SettingsFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.settings_view, container, false);


        LinearLayout duracionTimbre = view.findViewById(R.id.duracionTimbre);
        duracionTimbre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] opcionesDuracionTimbre = {"1 minute", "5 minutes", "10 minutes", "15 minutes", "20 minutes", "30 minutes"};
                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder.setTitle("Ring Duration");
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.setItems(opcionesDuracionTimbre, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        if(opcionesDuracionTimbre[i].equals("1 minute")){

                        }else{

                        }
                    }
                });

                Dialog dialog = builder.create();
                dialog.show();
            }
        });

        LinearLayout snoozeDuration = view.findViewById(R.id.configuracionRepeticion);
        snoozeDuration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                builder.setTitle("Ring Duration");
                builder.setView(inflater.inflate(R.layout.snooze_duration_settings_dialog, null));
                builder.setPositiveButton("Guardar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

                Dialog dialog = builder.create();
                dialog.show();
            }
        });


        return view;
    }
}
