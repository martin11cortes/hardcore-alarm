package co.edu.konradlorenz.hardcorealarm.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import co.edu.konradlorenz.hardcorealarm.R;
import co.edu.konradlorenz.hardcorealarm.adapters.ListadoAlarmasAdapter;
import co.edu.konradlorenz.hardcorealarm.entities.Alarma;

public class ListadoAlarmasFragment extends Fragment {
    FirebaseDatabase database = FirebaseDatabase.getInstance();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final DatabaseReference myRef = database.getReference();

        final View view = inflater.inflate(R.layout.listado_alarmas, container, false);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager().beginTransaction().add(R.id.fragment_container, new ConfiguracionAlarmaFragment()).addToBackStack(null).commit();
            }
        });

        ListView listadoAlarmas = view.findViewById(R.id.alarmas_list);
        listadoAlarmas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent nuevoIntent = new Intent(getContext(), ConfiguracionAlarmaFragment.class);
                startActivity(nuevoIntent);
            }
        });

        final ArrayList<Alarma> alarmas = new ArrayList<>();

        final ListView lista = view.findViewById(R.id.alarmas_list);

        myRef.child("alarms").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                final ArrayList daysArray = new ArrayList<>();
                for (DataSnapshot alarm : dataSnapshot.getChildren()) {
                    Alarma myAlarm = new Alarma();
                    myAlarm.setHour(Integer.parseInt(alarm.child("hour").getValue().toString()));
                    myAlarm.setMessage(alarm.child("message").getValue().toString());
                    myAlarm.setMinute(Integer.parseInt(alarm.child("minute").getValue().toString()));
                    myAlarm.setVibration(Boolean.parseBoolean(alarm.child("minute").getValue().toString()));
                    myAlarm.setVolume(Integer.parseInt(alarm.child("volume").getValue().toString()));
                    for (DataSnapshot day : alarm.child("days").getChildren()) {
                        daysArray.add(day.getValue().toString());
                    }
                    alarmas.add(myAlarm);
                }
                lista.setAdapter(new ListadoAlarmasAdapter(getActivity(), R.layout.alarma_item, alarmas));
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
//                Toast.makeText(view.getContext(), "Error: " + error, Toast.LENGTH_LONG).show();
            }
        });

        return view;

    }
}
