package co.edu.konradlorenz.hardcorealarm.entities;

import java.util.ArrayList;

public class User {
    public String name;

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
